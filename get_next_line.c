/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 04:28:23 by yskorode          #+#    #+#             */
/*   Updated: 2018/04/04 06:10:38 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

t_list		*ft_new_list(int fd, t_list **text)
{
	t_list		*tmp;

	tmp = *text;
	while (tmp)
	{
		if ((int)tmp->content_size == fd)
			return (tmp);
		tmp = tmp->next;
	}
	MAL_N((tmp = malloc(sizeof(t_list))));
	MAL_N((tmp->content = ft_strdup("\0")));
	tmp->content_size = fd;
	ft_lstadd(text, tmp);
	tmp = *text;
	return (tmp);
}

char		*ft_strjoin_ext(char *str1, char *str2)
{
	unsigned char	*ret;
	int				i;
	int				j;

	i = ft_strlen(str1);
	j = ft_strlen(str2);
	if (!(ret = (unsigned char*)ft_strnew(i + j)) || !str1 || !str2)
		return (NULL);
	i = 0;
	j = 0;
	while (str1[i])
	{
		ret[i] = str1[i];
		i++;
	}
	while (str2[j])
		ret[i++] = str2[j++];
	ret[i] = '\0';
	free(str1);
	return ((char*)ret);
}

char		*ft_strcpy_ext(char *line, char *str)
{
	int	j;
	int i;

	i = 0;
	j = 0;
	while (str[i] && str[i] != '\n')
		i++;
	if (!(line = ft_strnew(i)))
		return (0);
	while (str[j] && j < i)
	{
		line[j] = str[j];
		j++;
	}
	return (line);
}

char		*ft_freeing(char *str1, int count)
{
	char	*tmp;

	tmp = ft_strdup(str1 + count + 1);
	free(str1);
	return (tmp);
}

int			get_next_line(const int fd, char **line)
{
	static t_list	*list;
	int				ret;
	int				pos_n;
	t_list			*new;
	char			buf[BUFF_SIZE + 1];

	if (fd < 0 || !line || read(fd, buf, 0) < 0 || BUFF_SIZE <= 0 ||
		BUFF_SIZE >= BUFF_MAX || fd >= OPEN_MAX)
		return (-1);
	new = ft_new_list(fd, &list);
	while ((ret = read(fd, buf, BUFF_SIZE)))
	{
		buf[ret] = '\0';
		MAL_I((new->content = ft_strjoin_ext((char*)new->content, buf)));
		if (ft_strchr(new->content, '\n'))
			break ;
	}
	if (ret < BUFF_SIZE && ft_strlen(new->content) == 0)
		return (0);
	pos_n = ft_numstrchr(new->content, '\n');
	*line = ft_strcpy_ext(*line, new->content);
	(pos_n < (int)ft_strlen(new->content)) ?
	new->content = ft_freeing((char*)new->content, pos_n) :
	ft_bzero(new->content, pos_n + 1);
	return (1);
}
