/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 15:57:19 by yskorode          #+#    #+#             */
/*   Updated: 2018/04/04 04:36:02 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# define BUFF_SIZE 32
# define BUFF_MAX  8000000

# define MAL_N(x) {if (!x) {return (NULL);}}
# define MAL_I(x) {if (!x) {return (-1);}}

# include "libft/libft.h"
# include <sys/syslimits.h>

int get_next_line(const int fd, char **line);
#endif
