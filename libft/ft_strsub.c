/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/04 14:59:01 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/20 17:10:13 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	unsigned char	*str;
	size_t			i;

	str = NULL;
	if (s != NULL)
	{
		i = 0;
		str = malloc(sizeof(char) * len + 1);
		if (str != NULL)
		{
			while ((s[i] != '\0') && (i < len))
			{
				str[i] = (s[i + start]);
				i++;
			}
			str[i] = '\0';
		}
	}
	return ((char*)str);
}
