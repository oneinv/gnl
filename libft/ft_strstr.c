/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 20:21:43 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/15 08:21:37 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	int a;
	int b;

	a = 0;
	b = 0;
	if (ft_strlen(needle) == 0)
		return ((char*)haystack);
	while (haystack[a] != '\0')
	{
		b = 0;
		while (haystack[a + b] == needle[b])
		{
			b++;
			if (needle[b] == '\0')
				return ((char*)&haystack[a]);
		}
		a++;
	}
	return (NULL);
}
