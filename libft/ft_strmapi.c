/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/03 19:21:34 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/20 17:09:52 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*str;
	size_t	len;
	size_t	i;

	str = NULL;
	if (s != NULL)
	{
		len = ft_strlen((char*)s);
		str = ft_strnew(len);
		if (str != NULL)
		{
			i = 0;
			while (s[i] != '\0')
			{
				str[i] = f(i, s[i]);
				i++;
			}
		}
	}
	return (str);
}
