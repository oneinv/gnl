/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/24 12:16:11 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/20 15:13:57 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int c)
{
	int n;

	n = 0;
	while (str[n] != '\0')
		n++;
	while (n >= 0)
	{
		if (str[n] == (char)c)
			return ((char*)&str[n]);
		n--;
	}
	return (NULL);
}
