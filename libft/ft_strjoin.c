/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/04 15:33:32 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/20 17:11:12 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t			i;
	unsigned char	*res;

	res = NULL;
	if ((s1 != NULL) && (s2 != NULL))
	{
		res = malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2)));
		if (res != NULL)
		{
			i = 0;
			while (s1[i] != '\0')
			{
				res[i] = s1[i];
				i++;
			}
			i = 0;
			while (s2[i] != '\0')
			{
				res[i + ft_strlen(s1)] = s2[i];
				i++;
			}
			res[i + ft_strlen(s1)] = '\0';
		}
	}
	return ((char*)res);
}
