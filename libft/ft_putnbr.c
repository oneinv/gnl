/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 16:18:00 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/15 08:42:58 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int n)
{
	unsigned int temp;

	temp = 0;
	if (n < 0)
	{
		n = -n;
		ft_putchar('-');
	}
	temp = n;
	if (temp >= 10)
		ft_putnbr(temp / 10);
	ft_putchar((temp % 10) + '0');
}
