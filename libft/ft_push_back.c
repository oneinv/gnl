/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_back.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/20 20:14:41 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/20 20:23:55 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_push_back(t_list *head, void *content)
{
	t_list *last;
	t_list *tmp;

	last = ft_get_last(head);
	tmp = (t_list*)malloc(sizeof(t_list));
	tmp->content = content;
	tmp->next = NULL;
	last->next = tmp;
}
