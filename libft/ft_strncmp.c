/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 19:32:24 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/20 21:22:25 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t a;

	a = 0;
	if (!n)
		return (0);
	while (a < n && s1[a] != '\0' && (unsigned char)s1[a] ==
			(unsigned char)s2[a])
		a++;
	if (a < n && ((unsigned char*)s1)[a] != ((unsigned char*)s2)[a])
		return (((unsigned char*)s1)[a] - ((unsigned char*)s2)[a]);
	return (0);
}
