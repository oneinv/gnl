/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 18:19:15 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/21 16:13:23 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_digs(int n)
{
	int		i;

	i = 1;
	if (n < 0)
	{
		n = n * -1;
		i++;
	}
	while (n / 10)
	{
		n /= 10;
		i++;
	}
	return (i);
}

char			*ft_itoa(int n)
{
	int				i;
	unsigned int	tmp;
	char			*res;

	i = ft_digs(n);
	if ((res = (char*)malloc(sizeof(char) * i + 1)) == NULL)
		return (NULL);
	if (n < 0)
		tmp = n * -1;
	else
		tmp = n;
	res[i] = '\0';
	while (i-- >= 0)
	{
		if (tmp >= 10)
			res[i] = '0' + tmp % 10;
		else
			res[i] = '0' + tmp;
		tmp = tmp / 10;
	}
	if (n < 0)
		res[0] = '-';
	return (res);
}
