/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/19 17:36:12 by yskorode          #+#    #+#             */
/*   Updated: 2017/11/19 17:46:09 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   if dest is not large enough, program behavior is  unpredictable;
*/

char	*ft_strcat(char *s1, const char *s2)
{
	int a;
	int b;

	a = 0;
	b = 0;
	while (s1[a] != '\0')
		a++;
	while (s2[b] != '\0')
	{
		s1[a++] = s2[b];
		b++;
	}
	s1[a] = 0;
	return (s1);
}
