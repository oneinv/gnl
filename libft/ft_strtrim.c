/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/04 16:35:39 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/20 12:56:24 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int		ft_count(char const *s)
{
	int		flag;
	size_t	i;
	int		end;

	end = ft_strlen(s) - 1;
	i = 0;
	flag = 0;
	while (ft_is_space(s[i]) == 1)
	{
		flag++;
		i++;
		if (s[i + 1] == '\0')
			return (flag);
	}
	while (ft_is_space(s[end--]) == 1 && (end > 0))
		flag++;
	return (flag);
}

char			*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	j;
	char	*str;

	str = NULL;
	if (!s || !(str = (char*)malloc(sizeof(char) * ft_strlen(s) -
			ft_count(s) + 1)))
		return (NULL);
	j = 0;
	i = 0;
	while (ft_is_space(s[i]) == 1)
	{
		if (s[i] == '\0')
		{
			str[j] = '\0';
			return ((char*)str);
		}
		i++;
	}
	while (s[i] != '\0')
		str[j++] = s[i++];
	while (ft_is_space(str[j - 1]) == 1)
		j--;
	str[j] = '\0';
	return ((char*)str);
}
