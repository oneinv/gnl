/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/19 13:51:40 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/15 08:20:55 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	int a;
	int b;

	a = 0;
	b = 0;
	if (ft_strlen(needle) == 0)
		return ((char*)haystack);
	while (haystack[a] != '\0')
	{
		while (haystack[a] == needle[b])
		{
			a++;
			b++;
			if (a > (int)len)
				break ;
			if (needle[b] == '\0')
				return ((char*)&haystack[a - ft_strlen(needle)]);
		}
		a++;
	}
	return (NULL);
}
