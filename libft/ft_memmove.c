/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/25 13:37:29 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/14 17:54:08 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** тут бы малок поюзать, но кто его знает
*/

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char *s1;
	unsigned char *s2;

	s1 = (unsigned char *)dst;
	s2 = (unsigned char *)src;
	if (dst > src)
	{
		s1 = s1 + len;
		s2 = s2 + len;
		while (len--)
			*--s1 = *--s2;
	}
	else
		ft_memcpy(dst, src, len);
	return (dst);
}
