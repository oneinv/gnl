/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/20 13:45:29 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/20 18:03:38 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Пытаемся создать строку нужного размера, ретернит размер созданной строки
*/

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	j;
	int		len;

	i = ft_strlen(dst);
	len = ft_strlen(dst);
	j = 0;
	if (i >= size)
		return (size + ft_strlen(src));
	if (*src == '\0')
		return (size);
	while (i < size - 1)
		dst[i++] = src[j++];
	dst[i] = '\0';
	if (dst > 0)
		return (len + ft_strlen(src));
	return (0);
}
