/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yskorode <yskorode@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 18:06:20 by yskorode          #+#    #+#             */
/*   Updated: 2017/12/20 13:51:40 by yskorode         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list *add;

	add = malloc(sizeof(t_list));
	if (add != NULL)
	{
		if (content == NULL)
		{
			add->content_size = 0;
			add->content = NULL;
		}
		else
		{
			add->content_size = content_size;
			add->content = ft_memalloc(add->content_size);
			if (add->content == NULL)
				return (NULL);
			add->content = ft_memcpy(add->content, content, add->content_size);
		}
		add->next = NULL;
	}
	return (add);
}
